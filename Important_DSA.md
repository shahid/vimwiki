# DSA

## DS

1. Linked List
2. Trees, Tries and Graphs
3. Stacks & Queues
4. Heaps
5. Vectors/ArrayLists
6. Hash Table

## Algorithms

1. BFS
2. DFS
3. Binary Search
4. Merge Sort
5. Quick Sort

## Concepts

1. Bit Manipulation
2. Memory(Stack vs Heap)
3. Recursion
4. DP
5. Big O(Time & Space)
