---
title: Logic
author: Mohammed Shahid
date: 12/07/2022
fontfamily: ebgaramond
---

\usepackage{fontspec}
\usepackage{newunicodechar}

# Propositional logic 
Propositional logic (also called sentential logic)
is logic that includes sentence letters (A,B,C)
and logical connectives, but not quantifiers.
The semantics of propositional logic uses truth
assignments to the letters to determine whether a
compound propositional sentence is true.

* Propositional logic is decidable, for example by
the method of truth tables:

* Propositional logic is an axiomatization of Boolean logic.


# Predicate logic
Predicate logic is usually used as a synonym for
first-order logic, but sometimes it is used to
refer to other logics that have similar syntax.
Syntactically, first-order logic has the same
connectives as propositional logic, but it also
has variables for individual objects, quantifiers,
symbols for functions, and symbols for relations.
The semantics include a domain of discourse for
the variables and quantifiers to range over, along
with interpretations of the relation and function
symbols.


* Predicate logic (also called predicate calculus
and first-order logic) is an extension of
propositional logic to formulas involving terms
and predicates. The full predicate logic is
undecidable:

# Example:
 A proposition is a statement that is having a
truth value(either true or false) associated with
it. Where a predicate is a statement whose truth
value is dependent upon the variables.

__Example:__ P(n):n is an odd integer. Where a  n.
domain is a set of all integers. Here, P(n) is  n.
dependent upon Example 3+3=6 is a propositio    n.




In predicate logic, due to the availability of
individual variables, predicate letters and
quantifiers we have more "resources" to analyze
statements: instead of atoms like p,q etc, we
have Px,x=y,∀xQx,∃xRx that allow us to model
mathematical theorems and theories.


Propositional logic studies what is logically
true or implied on the basis of truth-functional
operators (∧, ∨, ¬, etc)

Predicate logic studies what is logically true or
implied on the basis of truth-functional operators
(∧, ∨, ¬, etc) and predicates, and individual
constants, and quantifiers (using variables).

If you throw in identity as well, you get
first-order logic (some people make a distinction
between predicate logic and first-order logic).
