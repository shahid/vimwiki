# YT-DLP

## Download certain parts of a playlist 
```
yt-dlp --playlist-start  --playlist-end  --playlist-items 10  -o "%(playlist_index)02d - %(title)s.%(ext)s"  
```


## Numbering Playlist
 -o "%(playlist_index)02d - %(title)s.%(ext)s"
