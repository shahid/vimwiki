# Youtube-DL

Download ***n*** th video from playlist
```
youtube-dl --playlist-items 10 <playlist_url>
```

Download specific  videos from playlist
```
youtube-dl --playlist-items 2,3,7,10 <playlist_url>
```

Download  range based videos from playlist

```
youtube-dl --playlist-start 10 <playlist_url>
```

Download  range based videos from playlist with full control
```
youtube-dl --playlist-start 2 --playlist-end 5 <playlist_url>
```
