---
title: Sufism a Gentle Introduction
author:
	 - Mohammed Shahid^[B.Tech Information Technology, shahid.xyz@yahoo.com]
fontfamily: ebgaramond
header-includes:
   - \usepackage{tcolorbox}

---

<!-- \sffamily\fboxrule.1em\fboxsep1em -->
<!-- \fcolorbox{black}{blue!50}{\color{white}} -->
<!-- \begin{minipage}[c][4cm][t]{4cm} -->
<!-- The ultimate goal of each tariqa is diffrent -->
<!-- usually to attain Wali or Haqiqa(Ultimate Truth). -->
<!-- Haqiqa - is subjective again depdends on Tariqa -->
<!-- but majority of them have same end goal. Some -->
<!-- people who follow _Ibn al-Arabi_ believing in -->
<!-- something like _Wahdatul Wujood_ or _Unity of -->
<!-- Existence_ which is -->
<!-- \end{minipage} -->


## Introduction to some Terimonology
Sufism as a word has a lot of interpretation
but people generally link the term to Islamic
msyticism, Islamic ascetic
Esoterisism. Simply means muslims who practice
mysticism or spirituality.

$$\hbox{Tariqa - Path(ie. you're journey to attain Ultimate Truth through Spirituality.)}$$
$$\hbox{Or}$$
$$\hbox{Tariqa is the bridge between the inner and outer worlds.}$$

---
__Note__
The ultimate goal of each tariqa is diffrent usually to attain Wali or Haqiqa(Ultimate Truth).
Haqiqa - is subjective again depdends on Tariqa but majority of them have same end goal.
Some people who follow _Ibn al-Arabi_ believing in something like _Wahdatul Wujood_ or _Unity of Existence_ which is 
completely _shirk_ from Traditional _Sunni_ point of view.
---

Murshid - Shaykh, Techer, Spiritual Guide,
				 
Murid - Student, Spiritual seeker, One who ready to travel his spiritual journey.

## Tariqa

All true Sufi tariqas have a lineage that is
traceable to Prophet Muhammad (peace be upon him)
through his son-in-law Hazreti Ali (may Allah be
pleased with him) or Hz. Abu Bakr (may Allah be
pleased with him).

The idea is that how to do _zikr_ (remembrence of allah) and becoming
wali of allah or becoming united back to allah
according to Quran and Hadith and some secret practices 
that only knows to prophet and his family members _(Ahl al-Bayt)_
and his close friend and father in law _Abu Bakr._

These methods were passed down through generations
and are the foundation upon which the Sufi tariqas
were later established.

The various Sufi tariqas (Sufi orders; literally,
paths) were established by their founders in
response to the needs of different kinds of
people. If a person is drawn to a particular
school of Sufism, such as the Qadiri Tariqa or
Mevlevi Tariqa, it is because that school has a
flavor or character that addresses the particular
needs of that individual. This can be thought
of in the same light as vocational schools. If
a person has a deep desire to become a chef of
Chinese cuisine, he does not go to a master of
French cuisine for training. Likewise, each
spiritual seeker has a particular character that
can be well served by a tariqa suited to his or
her personality.


While everyone is welcomed as a guest, it is
understood that a true shaykh will recognize the
people who belong to him or her and can only
accept his or her own spiritual “children" as
murids (students of Sufism; lit., committed ones).


Among the true tariqas there is general agreement
about the essential tenets of Sufism. There is no
competition among the tariqas. No tariqa claims
that they have the one true way, or that it is
higher than other orders.

## The Role of the Murhsid

Every true shaykh has been appointed to the task
of teaching tasawwuf, known in the West as Sufism,
by his own shaykh, who was previously assigned by
his shaykh, and so on, in a line that stretches
back to Prophet Muhammad (peace be upon him). Each
of the several schools, or tariqas, of Sufism,
is thus supported by its own ancestral lineage
of spiritual teachers, called silsila. Every
silsila is documented in writing, and each line
of descent, though distinct, proceeds from the
authoritative decree of Prophet Muhammad (pbuh).

In the act of initiation into a Sufi tariqa, the
murid (Sufi student) makes spiritual connection
not only with his own shaykh, but with the entire
chain of shaykhs within the Sufi order to which
his shaykh belongs. In taking hand with all of his
shaykh’s spiritual ancestors, the murid makes a
bond ultimately with the Prophet (pbuh) himself.
This connecting with his silsila is what Sufis
regard as “holding onto the rope” of Allah.



\fbox{
		Some very long text\\
		that would not be allowed\\
		in a frame box.}
    
    
\fbox{ab}
\fbox{\strut ab}
\fbox{$\mathstrut$ab}







p
