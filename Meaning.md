__Inference:__ A conclusion reached on the basis of
evidence and reasoning.

_According to Cambridge Dictionary:_

A guess that you make or an opinion that you form
based on the information that you have.

* They were warned to expect a heavy air attack
  and by inference many casualties.
* His change of mind was recent and sudden, the
  inference being that someone had persuaded him.
