= Git =
*Remove file from repo but not from filesystem*

git rm --cached dwm
git commit -m "Deleted file from repository only"
git push
