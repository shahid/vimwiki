---
title: Important Definitions
fontfamily: ebgaramond
---

# Free Thinker
Oxford English Dictionary, a freethinker is "a person who forms their own ideas and opinions rather than accepting those of other people, especially in religious teaching."

# Freethinking
 The Oxford English Dictionary defines freethinking as, "The free exercise of reason in matters of religious belief, unrestrained by deference to authority; the adoption of the principles of a free-thinker."

# Infrence
A conclusion reached  on the basis of evidence and reasoning.

# Rules of Inference

In the Philosophy of logic and Mathematical Logic a rule of inference, inference rule or
transformation rule is a 
