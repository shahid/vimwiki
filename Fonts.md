1. ebgaramond
2. mathdesign
3. garamondlibre

__Libertine__
\usepackage{libertine}
\usepackage{libertinust1math}
\usepackage[T1]{fontenc}


__Garamond:__
\usepackage[urw-garamond]{mathdesign}
\usepackage[T1]{fontenc}


__EBGaramond:__
\usepackage[cmintegrals,cmbraces]{newtxmath}
\usepackage{ebgaramond-maths}
\usepackage[T1]{fontenc}
