Unix - https://git.savannah.gnu.org/cgit/coreutils.git/tree/src
Decoding of GNU Coreutils - http://www.maizure.org/projects/decoded-gnu-coreutils/


# Important C - Librraies and Uses


```c
//Used for basic input/output stream
#include <stdio.h>
//Used for handling directory files
#include <dirent.h>
//For EXIT codes and error handling
#include <errno.h>
// Standard Library
#include <stdlib.h>
```


%p - printing Memory Address of a variable.
%d - printing integers.
%f - printing floating points.
%lf - printing double floating points.

